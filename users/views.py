from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated, NOT, SingleOperandHolder

from users.models import User
from users.serializers import UserSerializer


class UserViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [SingleOperandHolder(NOT, IsAuthenticated)]
