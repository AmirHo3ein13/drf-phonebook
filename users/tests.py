from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from users.models import User


class UserTests(APITestCase):
    def test_create_user(self):
        """
        Check user creation
        """

        data = {
            'email': 'test@mail.com',
            'password': 'test'
        }
        url = reverse('users-list')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        users = User.objects.all()
        self.assertEqual(users.count(), 1)
        self.assertEqual(users.first().email, data['email'])

    def test_user_sign_in(self):
        """
        Check user can sign in
        """

        data = {
            'email': 'test@mail.com',
            'password': 'test'
        }
        sign_up_url = reverse('users-list')
        self.client.post(sign_up_url, data, format='json')

        sign_in_url = reverse('token_obtain_pair')
        response = self.client.post(sign_in_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('access', response.data.keys())
