from django.urls import include, path
from rest_framework.routers import SimpleRouter
from rest_framework_extensions.routers import NestedRouterMixin

from contacts.views import ContactViewSet, NumberViewSet


class NestedDefaultRouter(NestedRouterMixin, SimpleRouter):
    pass


router = NestedDefaultRouter()

router_register = router.register(r'contacts', ContactViewSet, basename='contacts')
router_register.register('numbers', NumberViewSet, basename='numbers', parents_query_lookups=['contact'])

urlpatterns = [
    path('', include(router_register.router.urls)),
]
