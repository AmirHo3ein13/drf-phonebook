from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from contacts.models import Contact, Number
from users.models import User


class ContactsTest(APITestCase):
    def _get_tokens_for_user(self, user):
        refresh = RefreshToken.for_user(user)

        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }

    def _get_new_user(self, email: str = None):
        user = User.objects.create_user(email if email else 'test@mail.com', 'test')
        user.is_active = True
        user.save()
        return user

    def _authenticate_user(self, user: User):
        token = self._get_tokens_for_user(user)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token['access'])

    def test_create_contact(self):
        """
        Test creation of contact
        """

        data = {
            'name': 'test contact',
            'description': 'test description'
        }
        url = reverse('contacts-list')
        self._authenticate_user(self._get_new_user())
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['name'], data['name'])
        self.assertEqual(response.data['description'], data['description'])

    def test_get_contacts_list(self):
        """
        Get list of contacts of this user
        """

        user = self._get_new_user()

        contact1_data = {
            'name': 'test1',
            'number': '1'
        }
        contact1 = Contact(name=contact1_data['name'], description=contact1_data['name'], user_id=user.id)
        contact1.save()
        contact1_data['id'] = contact1.id

        Number.objects.bulk_create([
            Number(phone_number=contact1_data['number'], contact=contact1),
            Number(phone_number='2', contact=contact1)
        ])

        second_user = User.objects.create_user(email='temp@mail.com', password='test')
        second_user.save()
        Contact.objects.bulk_create([
            Contact(name='test2', description='test2', user_id=user.id),
            Contact(name='test3', description='test3', user=second_user)
        ])

        url = reverse('contacts-list')
        self._authenticate_user(user)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertIn(contact1_data, response.data)

    def test_get_contact(self):
        """
        Get single contact
        """

        user = self._get_new_user()

        contact1_data = {
            'name': 'test1',
            'description': None,
            'numbers': []
        }

        contact1 = Contact(name=contact1_data['name'], user_id=user.id)
        contact1.save()
        contact1_data['id'] = contact1.id

        numbers = [Number(phone_number=str(i), contact=contact1) for i in range(2)]
        Number.objects.bulk_create(numbers)

        for number in numbers:
            contact1_data['numbers'].append({
                'id': number.id,
                'phone_number': number.phone_number
            })

        url = reverse('contacts-detail', kwargs={'pk': contact1.id})

        self._authenticate_user(user)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, contact1_data)

    def test_update_contact(self):
        """
        Update contact and check it's been updated
        """

        user = self._get_new_user()

        contact1_data = {
            'name': 'test1',
            'description': None,
            'user': user.id
        }
        contact1 = Contact(name=contact1_data['name'], user_id=user.id)
        contact1.save()

        url = reverse('contacts-detail', kwargs={'pk': contact1.id})

        new_data = {
            'name': 'test2',
            'description': 'test'
        }
        self._authenticate_user(user)
        response = self.client.put(url, data=new_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_contact = Contact.objects.get(pk=contact1.id)
        new_contact_data = {
            'name': new_contact.name,
            'description': new_contact.description
        }
        self.assertEqual(new_data, new_contact_data)

    def test_delete_contact(self):
        """
        Delete contact and check it's been deleted
        """

        user = self._get_new_user()

        contact1_data = {
            'name': 'test1',
            'description': None,
            'user': user.id
        }
        contact1 = Contact(name=contact1_data['name'],
                           description=contact1_data['description'],
                           user_id=user.id)
        contact1.save()

        url = reverse('contacts-detail', kwargs={'pk': contact1.id})

        self._authenticate_user(user)
        response = self.client.delete(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Contact.objects.filter(pk=contact1.id).exists(), False)

    def test_search_contact(self):
        """
        Search contacts with name or numbers
        """

        user1 = self._get_new_user()
        user2 = self._get_new_user('test2@mail.com')

        test_query = '1'
        contacts_list = [
            Contact(name=test_query, user=user1),
            Contact(name='test2', user=user1),
            Contact(name='test3', user=user1),
            Contact(name=test_query, user=user2),
        ]
        Contact.objects.bulk_create(contacts_list)

        number = Number(phone_number=test_query, contact=contacts_list[1])
        number.save()

        accepted_result_contact0 = {
            'id': contacts_list[0].id,
            'name': contacts_list[0].name,
            'number': None,
        }
        accepted_result_contact1 = {
            'id': contacts_list[1].id,
            'name': contacts_list[1].name,
            'number': number.phone_number,
        }

        self._authenticate_user(user1)
        url = reverse('contacts-list')
        response = self.client.get(url, data={'search': test_query}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data, [accepted_result_contact0, accepted_result_contact1])

