from django.contrib import admin

# Register your models here.
from contacts.models import Contact, Number

admin.site.register(Contact)
admin.site.register(Number)
