from rest_framework import viewsets, filters, mixins, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_extensions.mixins import NestedViewSetMixin

from contacts.models import Number
from contacts.permissions import IsContactOwner, IsNumberOwner
from contacts.serializers import ContactSerializer, ContactListSerializer, NumberSerializer


class ContactViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, IsContactOwner]
    filter_backends = [filters.SearchFilter]
    search_fields = ['=numbers__phone_number', '=name']

    def get_queryset(self):
        return self.request.user.contact_set.all()

    def create(self, request, *args, **kwargs):
        numbers = request.data.pop('numbers', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        if numbers:
            for number in numbers:
                number['contact'] = serializer.instance.pk
            numbers_serializer = NumberSerializer(data=numbers, many=True)
            numbers_serializer.is_valid(raise_exception=True)
            numbers_serializer.save()

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_serializer_class(self):
        if self.action == 'list':
            return ContactListSerializer
        else:
            return ContactSerializer


class NumberViewSet(NestedViewSetMixin,
                    viewsets.GenericViewSet,
                    mixins.CreateModelMixin,
                    mixins.DestroyModelMixin,
                    mixins.UpdateModelMixin):
    permission_classes = [IsAuthenticated, IsNumberOwner]
    queryset = Number.objects.all()
    serializer_class = NumberSerializer
