from rest_framework import serializers

from contacts.models import Contact, Number


class NumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Number
        fields = '__all__'
        extra_kwargs = {
            'contact': {'write_only': True}
        }


class ContactListSerializer(serializers.ModelSerializer):
    number = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Contact
        fields = ['id', 'name', 'number']

    def get_number(self, instance):
        f_number = instance.numbers.first()
        if f_number:
            return f_number.phone_number
        else:
            return None


class ContactSerializer(serializers.ModelSerializer):
    numbers = NumberSerializer(many=True, read_only=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Contact
        fields = '__all__'
