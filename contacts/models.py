from django.db import models

from users.models import User


class Contact(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Number(models.Model):
    phone_number = models.CharField(max_length=14)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, related_name='numbers')

    def __str__(self):
        return self.phone_number
