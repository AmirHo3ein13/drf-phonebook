djangorestframework>=3.11.0
django>=3.0.5
djangorestframework_simplejwt>=4.4.0
psycopg2-binary>=2.8.5
drf-extensions>=0.6.0
coverage>=5.1